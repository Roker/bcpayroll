﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BCPayroll.model
{
    internal class OTHours
    {
        public double Multiplier { get; private set; }
        public double Hours { get; set; }

        internal OTHours(double multiplier)
        {
            Multiplier = multiplier;
        }
    }
}
