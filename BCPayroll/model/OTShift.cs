﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace BCPayroll.model
{
    internal class OTShift : Shift
    {
        private Dictionary<double, OTHours> _multipliedHours = new Dictionary<double, OTHours>();

        internal OTShift(DateTime startTime) : base(startTime)
        {
            InitRegularHours();
        }

        internal OTShift(DateTime startTime, double hours) : base(startTime, hours)
        {
            InitRegularHours();
        }

        internal OTShift(DateTime startTime, double hours, string wageId) : base(startTime, hours, wageId)
        {
            InitRegularHours();
        }

        private void InitRegularHours()
        {
            SetOTHours(new OTHours(1.0) { Hours = this.Hours });
        }

        public OTShift(Shift shift) : this(shift.StartTime, shift.Hours, shift.WageId) { }


        internal void AddOTHours(OTHours newHours)
        {
            OTHours regHours = GetOTHours(1.0);
            if(regHours.Hours >= newHours.Hours)
            {
                regHours.Hours -= newHours.Hours;

                OTHours otHours = GetOTHours(newHours.Multiplier);
                otHours.Hours += newHours.Hours;
            }
            else
            {
                throw new InvalidOperationException("Not enough regular hours remaining in shift");
            }
        }

        internal double GetMultipliedHours(double multiplier)
        {
            return GetOTHours(multiplier).Hours;
        }

        private void SetOTHours(OTHours newOtHours)
        {
            _multipliedHours[newOtHours.Multiplier] = newOtHours;
        }
        private OTHours GetOTHours(double multiplier)
        {
            if (_multipliedHours.ContainsKey(multiplier))
            {
                return _multipliedHours[multiplier];
            }

            OTHours newOTHours = new OTHours(multiplier);
            _multipliedHours[multiplier] = newOTHours;

            return newOTHours;
        }
    }
}
