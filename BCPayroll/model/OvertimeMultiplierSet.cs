﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BCPayroll.model
{
    public class OvertimeMultiplierSet
    {
        public List<OvertimeMultiplier> DailyOvertimeMultipliers { get; set; } = new List<OvertimeMultiplier>();
        public WeeklyOvertimeMultiplier WeeklyOvertimeMultiplier { get; set; }
    }
}
