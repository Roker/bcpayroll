﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BCPayroll.model
{
    public class Shift : IComparable<Shift>
    {
        public DateTime StartTime { get; protected set; }

        public double Hours { get; protected set; }

        public string WageId { get; protected set; }
        public DateTime Date => StartTime.Date;

        public Shift(DateTime startTime)
        {
            StartTime = startTime;
        }

        public Shift(DateTime startTime, double hours) : this(startTime)
        {
            Hours = hours;
        }

        public Shift(DateTime startTime, double hours, string wageId) : this(startTime, hours)
        {
            WageId = wageId;
        }

        public int CompareTo(Shift other)
        {
            if (other == null) return 1;
            return StartTime.CompareTo(other.StartTime);
        }

    }
}
