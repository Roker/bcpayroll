﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BCPayroll.model
{
    public class WeeklyOvertimeMultiplier : OvertimeMultiplier
    {
        public WeeklyOvertimeMultiplier(int minHours, double multiplier, int maxDailyHours)
            : base(minHours, multiplier)
        {
            MaxDailyHours = maxDailyHours;
        }

        public int MaxDailyHours { get; set; }
    }
}
