﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BCPayroll.model
{
    public class OvertimeMultiplier : IComparable<OvertimeMultiplier>
    {
        public double MinHours { get; set; }
        public double Multiplier { get; set; }

        public OvertimeMultiplier(double minHours, double multiplier)
        {
            MinHours = minHours;
            Multiplier = multiplier;
        }

        public int CompareTo(OvertimeMultiplier other)
        {
            if (other == null) return 1;

            return other.MinHours.CompareTo(MinHours);
        }
    }
}
