﻿using BCPayroll.model;
using Roker.Extensions;
using Roker.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BCPayroll.util
{
    internal static class PayrollCalculationFunctions
    {
        private const double DEFAULT_HOLIDAY_OT_MULTIPLIER = 1.5;
        internal static OTShift CalcDailyOT(Shift shift, List<OvertimeMultiplier> dailyOvertimeMultipliers)
        {
            if (dailyOvertimeMultipliers == null) throw new ArgumentNullException("List of multipliers null");
            if (shift == null) throw new ArgumentNullException("Shift null");

            double hours = shift.Hours;

            //prevent threading issues
            dailyOvertimeMultipliers = dailyOvertimeMultipliers.ToList();
            dailyOvertimeMultipliers.Sort();

            OTShift multShift = new OTShift(shift);

            foreach (OvertimeMultiplier overtimeMultiplier in dailyOvertimeMultipliers)
            {
                if (hours > overtimeMultiplier.MinHours)
                {
                    double otHours = hours - overtimeMultiplier.MinHours;
                    hours -= otHours;

                    multShift.AddOTHours(new OTHours(overtimeMultiplier.Multiplier) { Hours = otHours });
                }
            }

            return multShift;
        }

        internal static List<OTShift> CalcDailyOT(List<Shift> shifts, List<OvertimeMultiplier> dailyOvertimeMultipliers)
        {
            if (shifts == null) throw new ArgumentNullException("List of shifts null");
            if (dailyOvertimeMultipliers == null) throw new ArgumentNullException("List of multipliers null");

            List<OTShift> otShifts = new List<OTShift>();
            foreach(Shift shift in shifts){
                otShifts.Add(CalcDailyOT(shift, dailyOvertimeMultipliers));
            }
            return otShifts;
        }

        /*
         * MODIFIES: otshifts - converts some reg hours to weekly OT hours if required
         */
        internal static void CalcWeeklyOT(List<OTShift> otShifts, 
            WeeklyOvertimeMultiplier weeklyOvertimeMultiplier, DayOfWeek startOfWeek = DayOfWeek.Sunday)
        {
            if (weeklyOvertimeMultiplier == null) throw new ArgumentNullException("weeklyOvertimeMultiplier null");

            List<List<OTShift>> shiftsByWeek = SeperateShiftsByWeek(otShifts, startOfWeek);
            foreach(List<OTShift> weekOfShifts in shiftsByWeek)
            {
                CalcSingleWeekOT(weekOfShifts, weeklyOvertimeMultiplier);
            }
        }

        internal static List<OTShift> CalcDailyAndWeeklyOT(List<Shift> shifts,
            OvertimeMultiplierSet overtimeMultiplierSet, DayOfWeek startOfWeek = DayOfWeek.Sunday)
        {
            if (overtimeMultiplierSet == null) throw new ArgumentNullException("OvertimeMultiplierSet is null");
            List<OTShift> otShifts = CalcDailyOT(shifts, overtimeMultiplierSet.DailyOvertimeMultipliers);
            CalcWeeklyOT(otShifts, overtimeMultiplierSet.WeeklyOvertimeMultiplier, startOfWeek);

            return otShifts;
        }

        /*
         * MODIFIES: otshifts - converts some reg hours to weekly OT hours if required
         */
        private static void CalcSingleWeekOT(List<OTShift> otShifts,
            WeeklyOvertimeMultiplier weeklyOvertimeMultiplier)
        {
            double weekHours = 0;
            foreach (OTShift otShift in otShifts)
            {
                weekHours += Math.Min(otShift.Hours, weeklyOvertimeMultiplier.MaxDailyHours);

                if(weekHours > weeklyOvertimeMultiplier.MinHours)
                {
                    double hoursPastMinimum = weekHours - weeklyOvertimeMultiplier.MinHours;
                    double regHoursInShift = otShift.GetMultipliedHours(1.0);

                    double otHours = Math.Min(hoursPastMinimum, regHoursInShift);

                    otShift.AddOTHours(new OTHours(weeklyOvertimeMultiplier.Multiplier) { Hours = otHours });

                    weekHours = weeklyOvertimeMultiplier.MinHours;
                }
            }
        }
        /*
         * Calculate OT hours as a result of holidays
         * 
         * Does not calculate avg days pay
         * */
        public static void CalcHolidayOT(List<OTShift> otShifts, List<DateTime> holidays,
            double holidayMultiplier = DEFAULT_HOLIDAY_OT_MULTIPLIER)
        {
            foreach(DateTime holiday in holidays)
            {
                CalcHolidayOT(otShifts, holiday, holidayMultiplier);
            }
        }

        public static void CalcHolidayOT(List<OTShift> otShifts, DateTime holiday,
            double holidayMultiplier = DEFAULT_HOLIDAY_OT_MULTIPLIER)
        {
            foreach (OTShift otShift in otShifts)
            {
                if (otShift.Date == holiday.Date)
                {
                    //Set remaining reg hours as OT
                    otShift.AddOTHours(new OTHours(holidayMultiplier) { Hours = otShift.GetMultipliedHours(1.0) });
                }
            }
        }

        private static List<List<OTShift>> SeperateShiftsByWeek(List<OTShift> otShifts, DayOfWeek firstDayOfWeek)
        {
            return DateTimeUtil.SeperateByWeek<OTShift>(otShifts, (x) => x.Date, firstDayOfWeek);
        }
    }
}
