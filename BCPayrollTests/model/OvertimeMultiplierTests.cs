﻿using BCPayroll.model;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace BCPayroll.Tests.model
{
    public class OvertimeMultiplierTests
    {
        private OvertimeMultiplier _mult0 = new OvertimeMultiplier(12, 0);
        private OvertimeMultiplier _mult1 = new OvertimeMultiplier(10, 0);
        private OvertimeMultiplier _mult2 = new OvertimeMultiplier(8, 0);
        [Fact]
        public void SortTest1()
        {
            List<OvertimeMultiplier> multipliers = new List<OvertimeMultiplier>();

            multipliers.Add(_mult0);
            multipliers.Add(_mult1);
            multipliers.Add(_mult2);

            multipliers.Sort();

            Assert.Equal(_mult0, multipliers[0]);
            Assert.Equal(_mult1, multipliers[1]);
            Assert.Equal(_mult2, multipliers[2]);
        }

        [Fact]
        public void SortTest2()
        {
            List<OvertimeMultiplier> multipliers = new List<OvertimeMultiplier>();

            multipliers.Add(_mult0);
            multipliers.Add(_mult2);
            multipliers.Add(_mult1);

            multipliers.Sort();

            Assert.Equal(_mult0, multipliers[0]);
            Assert.Equal(_mult1, multipliers[1]);
            Assert.Equal(_mult2, multipliers[2]);
        }

        [Fact]
        public void SortTest3()
        {
            List<OvertimeMultiplier> multipliers = new List<OvertimeMultiplier>();

            multipliers.Add(_mult1);
            multipliers.Add(_mult0);
            multipliers.Add(_mult2);

            multipliers.Sort();

            Assert.Equal(_mult0, multipliers[0]);
            Assert.Equal(_mult1, multipliers[1]);
            Assert.Equal(_mult2, multipliers[2]);
        }

        [Fact]
        public void SortTest4()
        {
            List<OvertimeMultiplier> multipliers = new List<OvertimeMultiplier>();

            multipliers.Add(_mult1);
            multipliers.Add(_mult2);
            multipliers.Add(_mult0);

            multipliers.Sort();

            Assert.Equal(_mult0, multipliers[0]);
            Assert.Equal(_mult1, multipliers[1]);
            Assert.Equal(_mult2, multipliers[2]);
        }

        [Fact]
        public void SortTest5()
        {
            List<OvertimeMultiplier> multipliers = new List<OvertimeMultiplier>();

            multipliers.Add(_mult2);
            multipliers.Add(_mult0);
            multipliers.Add(_mult1);

            multipliers.Sort();

            Assert.Equal(_mult0, multipliers[0]);
            Assert.Equal(_mult1, multipliers[1]);
            Assert.Equal(_mult2, multipliers[2]);
        }

        [Fact]
        public void SortTest6()
        {
            List<OvertimeMultiplier> multipliers = new List<OvertimeMultiplier>();

            multipliers.Add(_mult2);
            multipliers.Add(_mult1);
            multipliers.Add(_mult0);

            multipliers.Sort();

            Assert.Equal(_mult0, multipliers[0]);
            Assert.Equal(_mult1, multipliers[1]);
            Assert.Equal(_mult2, multipliers[2]);
        }
    }
}
