using BCPayroll.model;
using System;
using System.Collections.Generic;
using Xunit;

namespace BCPayroll.Tests.model
{
    public class ShiftTests
    {
        [Fact]
        public void SortTest()
        {
            List<Shift> shifts = new List<Shift>();
            Shift shift0 = new Shift(DateTime.Now);
            Shift shift1 = new Shift(shift0.StartTime.AddDays(1));
            Shift shift2 = new Shift(shift0.StartTime.AddDays(2));
            Shift shift3 = new Shift(shift0.StartTime.AddDays(3));
            Shift shift4 = new Shift(shift0.StartTime.AddDays(4));

            shifts.Add(shift4);
            shifts.Add(shift2);
            shifts.Add(shift3);
            shifts.Add(shift1);
            shifts.Add(shift0);

            shifts.Sort();

            Assert.Equal(shift0, shifts[0]);
            Assert.Equal(shift1, shifts[1]);
            Assert.Equal(shift2, shifts[2]);
            Assert.Equal(shift3, shifts[3]);
            Assert.Equal(shift4, shifts[4]);
        }
    }
}
