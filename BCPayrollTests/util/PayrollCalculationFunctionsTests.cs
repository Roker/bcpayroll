﻿using BCPayroll.model;
using BCPayroll.util;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace BCPayroll.Tests.util
{
    public class PayrollCalculationFunctionsTests
    {
        private static OvertimeMultiplierSet _standardOTSet = new OvertimeMultiplierSet();
        static PayrollCalculationFunctionsTests()
        {
            _standardOTSet.DailyOvertimeMultipliers.Add(new OvertimeMultiplier(8, 1.5));
            _standardOTSet.DailyOvertimeMultipliers.Add(new OvertimeMultiplier(12, 2));

            _standardOTSet.WeeklyOvertimeMultiplier = new WeeklyOvertimeMultiplier(40, 1.5, 8);

        }

        private static void OTShiftAssertions(OTShift otShift, double totalHours, double regHours, double oneFiveHours, double twoHours)
        {
            Assert.Equal(totalHours, otShift.Hours);
            Assert.Equal(regHours, otShift.GetMultipliedHours(1.0));
            Assert.Equal(oneFiveHours, otShift.GetMultipliedHours(1.5));
            Assert.Equal(twoHours, otShift.GetMultipliedHours(2.0));
        }

        public class CalcDailyOTTests
        {
            [Fact]
            public void BasicTest()
            {

                Shift shift = new Shift(DateTime.UtcNow, 14);

                OTShift multShift = PayrollCalculationFunctions.CalcDailyOT(shift, _standardOTSet.DailyOvertimeMultipliers);

                OTShiftAssertions(multShift, 14, 8, 4, 2);
            }

            [Fact]
            public void NoOTTest()
            {
                Shift shift = new Shift(DateTime.UtcNow, 8);

                OTShift multShift = PayrollCalculationFunctions.CalcDailyOT(shift, _standardOTSet.DailyOvertimeMultipliers);

                OTShiftAssertions(multShift, 8, 8, 0, 0);
            }

            [Fact]
            public void NullMultipliersTest()
            {
                Shift shift = new Shift(DateTime.UtcNow, 8);

                Assert.Throws(new ArgumentNullException().GetType(), () => PayrollCalculationFunctions.CalcDailyOT(shift, null));
            }

            [Fact]
            public void NullShiftsTest()
            {
                Shift shift = null;

                Assert.Throws(new ArgumentNullException().GetType(),
                    () => PayrollCalculationFunctions.CalcDailyOT(shift, _standardOTSet.DailyOvertimeMultipliers));
            }
        }


        public class CalcWeeklyOTTests
        {
            [Fact]
            public void BasicTest()
            {
                List<Shift> shifts = new List<Shift>();

                DateTime monday = new DateTime(637266206020000000);
                shifts.Add(new Shift(monday, 8)); // monday 8 hr shift
                shifts.Add(new Shift(monday.AddDays(1), 10)); // tuesday 10 hr shift
                shifts.Add(new Shift(monday.AddDays(2), 10)); // wednesday 10 hr shift
                shifts.Add(new Shift(monday.AddDays(3), 12)); // thursday 12 hr shift
                shifts.Add(new Shift(monday.AddDays(4), 10)); // friday 10 hr shift
                shifts.Add(new Shift(monday.AddDays(5), 8)); // saturday 10 hr shift

                List<OTShift> otShifts = PayrollCalculationFunctions.CalcDailyOT(shifts, _standardOTSet.DailyOvertimeMultipliers);

                PayrollCalculationFunctions.CalcWeeklyOT(otShifts, _standardOTSet.WeeklyOvertimeMultiplier);

                OTShiftAssertions(otShifts[0], 8, 8, 0, 0);
                OTShiftAssertions(otShifts[1], 10, 8, 2, 0);
                OTShiftAssertions(otShifts[2], 10, 8, 2, 0);
                OTShiftAssertions(otShifts[3], 12, 8, 4, 0);
                OTShiftAssertions(otShifts[4], 10, 8, 2, 0);
                OTShiftAssertions(otShifts[5], 8, 0, 8, 0);
            }

            [Fact]
            public void CrossWeekTest()
            {
                List<Shift> shifts = new List<Shift>();

                DateTime friday = new DateTime(637269662020000000);
                shifts.Add(new Shift(friday, 8)); // friday 8 hr shift
                shifts.Add(new Shift(friday.AddDays(1), 10)); // saturday 10 hr shift
                shifts.Add(new Shift(friday.AddDays(2), 10)); // sunday 10 hr shift -- week cutoff
                shifts.Add(new Shift(friday.AddDays(3), 12)); // monday 12 hr shift
                shifts.Add(new Shift(friday.AddDays(4), 10)); // tuesday 10 hr shift
                shifts.Add(new Shift(friday.AddDays(5), 14)); // wednesday 14 hr shift

                List<OTShift> otShifts = PayrollCalculationFunctions.CalcDailyOT(shifts, _standardOTSet.DailyOvertimeMultipliers);
                PayrollCalculationFunctions.CalcWeeklyOT(otShifts, _standardOTSet.WeeklyOvertimeMultiplier);


                OTShiftAssertions(otShifts[0], 8, 8, 0, 0);
                OTShiftAssertions(otShifts[1], 10, 8, 2, 0);
                OTShiftAssertions(otShifts[2], 10, 8, 2, 0);
                OTShiftAssertions(otShifts[3], 12, 8, 4, 0);
                OTShiftAssertions(otShifts[4], 10, 8, 2, 0);
                OTShiftAssertions(otShifts[5], 14, 8, 4, 2);
            }

            [Fact]
            public void CustomStartOfWeekTest()
            {
                List<Shift> shifts = new List<Shift>();

                DateTime friday = new DateTime(637269662020000000);
                shifts.Add(new Shift(friday, 8)); // friday 8 hr shift -- week cutoff
                shifts.Add(new Shift(friday.AddDays(1), 10)); // saturday 10 hr shift
                shifts.Add(new Shift(friday.AddDays(2), 10)); // sunday 10 hr shift 
                shifts.Add(new Shift(friday.AddDays(3), 12)); // monday 12 hr shift
                shifts.Add(new Shift(friday.AddDays(4), 10)); // tuesday 10 hr shift
                shifts.Add(new Shift(friday.AddDays(5), 14)); // wednesday 14 hr shift

                List<OTShift> otShifts = PayrollCalculationFunctions.CalcDailyOT(shifts, _standardOTSet.DailyOvertimeMultipliers);
                PayrollCalculationFunctions.CalcWeeklyOT(otShifts, _standardOTSet.WeeklyOvertimeMultiplier, DayOfWeek.Friday);

                OTShiftAssertions(otShifts[0], 8, 8, 0, 0);
                OTShiftAssertions(otShifts[1], 10, 8, 2, 0);
                OTShiftAssertions(otShifts[2], 10, 8, 2, 0);
                OTShiftAssertions(otShifts[3], 12, 8, 4, 0);
                OTShiftAssertions(otShifts[4], 10, 8, 2, 0);
                OTShiftAssertions(otShifts[5], 14, 0, 12, 2);
            }

            [Fact]
            public void NullShiftsTest()
            {
                List<Shift> shifts = null;
                Assert.Throws(new ArgumentNullException().GetType(),
                    () => PayrollCalculationFunctions.CalcDailyOT(shifts, _standardOTSet.DailyOvertimeMultipliers));
            }

            [Fact]
            public void NullMultipliersTest()
            {
                List<Shift> shifts = new List<Shift>();

                DateTime friday = new DateTime(637269662020000000);
                shifts.Add(new Shift(friday, 8)); // friday 8 hr shift -- week cutoff
                shifts.Add(new Shift(friday.AddDays(1), 10)); // saturday 10 hr shift
                shifts.Add(new Shift(friday.AddDays(2), 10)); // sunday 10 hr shift 
                shifts.Add(new Shift(friday.AddDays(3), 12)); // monday 12 hr shift
                shifts.Add(new Shift(friday.AddDays(4), 10)); // tuesday 10 hr shift
                shifts.Add(new Shift(friday.AddDays(5), 14)); // wednesday 14 hr shift


                Assert.Throws(new ArgumentNullException().GetType(),
                    () => PayrollCalculationFunctions.CalcDailyOT(shifts, null));
            }
        }

        public class CalcDailyAndWeeklyOTTests
        {
            [Fact]
            public void BasicTest()
            {
                List<Shift> shifts = new List<Shift>();

                DateTime sunday = new DateTime(637295460070000000);
                shifts.Add(new Shift(sunday, 10)); //8
                shifts.Add(new Shift(sunday.AddDays(1), 4));//12
                shifts.Add(new Shift(sunday.AddDays(2), 15));//20
                shifts.Add(new Shift(sunday.AddDays(3), 12)); //28
                shifts.Add(new Shift(sunday.AddDays(4), 8)); //36
                shifts.Add(new Shift(sunday.AddDays(5), 7)); //43
                shifts.Add(new Shift(sunday.AddDays(6), 14));

                List<OTShift> otShifts = PayrollCalculationFunctions.CalcDailyAndWeeklyOT(shifts, _standardOTSet);

                OTShiftAssertions(otShifts[0], 10, 8, 2, 0);
                OTShiftAssertions(otShifts[1], 4, 4, 0, 0);
                OTShiftAssertions(otShifts[2], 15, 8, 4, 3);
                OTShiftAssertions(otShifts[3], 12, 8, 4, 0);
                OTShiftAssertions(otShifts[4], 8, 8, 0, 0);
                OTShiftAssertions(otShifts[5], 7, 4, 3, 0);
                OTShiftAssertions(otShifts[6], 14, 0, 12, 2);
            }

            [Fact]
            public void NullSetTest()
            {
                List<Shift> shifts = new List<Shift>();

                DateTime sunday = new DateTime(637295460070000000);
                shifts.Add(new Shift(sunday, 10)); //8 -- comments are indicating limited weekly hours
                shifts.Add(new Shift(sunday.AddDays(1), 4));//12
                shifts.Add(new Shift(sunday.AddDays(2), 15));//20
                shifts.Add(new Shift(sunday.AddDays(3), 12)); //28
                shifts.Add(new Shift(sunday.AddDays(4), 8)); //36
                shifts.Add(new Shift(sunday.AddDays(5), 7)); //43
                shifts.Add(new Shift(sunday.AddDays(6), 14));

                Assert.Throws(new ArgumentNullException().GetType(),
                    () => PayrollCalculationFunctions.CalcDailyAndWeeklyOT(shifts, null));
            }

            [Fact]
            public void AveragingAgreementTest()
            {
                OvertimeMultiplierSet averagingSet = new OvertimeMultiplierSet();
                averagingSet.WeeklyOvertimeMultiplier = new WeeklyOvertimeMultiplier(40, 1.5, 24);

                averagingSet.DailyOvertimeMultipliers.Add(new OvertimeMultiplier(12, 2));

                List<Shift> shifts = new List<Shift>();

                DateTime sunday = new DateTime(637295460070000000);
                shifts.Add(new Shift(sunday, 10)); //10
                shifts.Add(new Shift(sunday.AddDays(1), 4));//14
                shifts.Add(new Shift(sunday.AddDays(2), 12));//26
                shifts.Add(new Shift(sunday.AddDays(3), 14));//40
                shifts.Add(new Shift(sunday.AddDays(4), 8)); //48
                shifts.Add(new Shift(sunday.AddDays(5), 7)); //55

                List<OTShift> otShifts = PayrollCalculationFunctions.CalcDailyAndWeeklyOT(shifts, averagingSet);

                OTShiftAssertions(otShifts[0], 10, 10, 0, 0);
                OTShiftAssertions(otShifts[1], 4, 4, 0, 0);
                OTShiftAssertions(otShifts[2], 12, 12, 0, 0);
                OTShiftAssertions(otShifts[3], 14, 12, 0, 2);
                OTShiftAssertions(otShifts[4], 8, 0, 8, 0);
                OTShiftAssertions(otShifts[5], 7, 0, 7, 0);
            }
        }


        public class CalcHolidayOTTests
        {
            [Fact]
            public void BasicTest()
            {
                DateTime sunday = new DateTime(637295460070000000);
                List<Shift> shifts = new List<Shift>();
                shifts.Add(new Shift(sunday, 8));
                shifts.Add(new Shift(sunday.AddDays(1), 8)); //holiday
                shifts.Add(new Shift(sunday.AddDays(2), 8));


                List<OTShift> otShifts = PayrollCalculationFunctions.CalcDailyAndWeeklyOT(shifts, _standardOTSet);

                PayrollCalculationFunctions.CalcHolidayOT(otShifts, sunday.AddDays(1));

                OTShiftAssertions(otShifts[0], 8, 8, 0, 0);
                OTShiftAssertions(otShifts[1], 8, 0, 8, 0);
                OTShiftAssertions(otShifts[2], 8, 8, 0, 0);

            }

            [Fact]
            public void WithDailyOTTest()
            {
                DateTime sunday = new DateTime(637295460070000000);
                List<Shift> shifts = new List<Shift>();
                shifts.Add(new Shift(sunday, 8));
                shifts.Add(new Shift(sunday.AddDays(1), 10)); //holiday
                shifts.Add(new Shift(sunday.AddDays(2), 14));


                List<OTShift> otShifts = PayrollCalculationFunctions.CalcDailyAndWeeklyOT(shifts, _standardOTSet);

                PayrollCalculationFunctions.CalcHolidayOT(otShifts, sunday.AddDays(1));

                OTShiftAssertions(otShifts[0], 8, 8, 0, 0);
                OTShiftAssertions(otShifts[1], 10, 0, 10, 0);
                OTShiftAssertions(otShifts[2], 14, 8, 4, 2);
            }

            [Fact]
            public void WithMoreDailyOTTest()
            {
                DateTime sunday = new DateTime(637295460070000000);
                List<Shift> shifts = new List<Shift>();
                shifts.Add(new Shift(sunday, 8));
                shifts.Add(new Shift(sunday.AddDays(1), 10));
                shifts.Add(new Shift(sunday.AddDays(2), 14)); //holiday


                List<OTShift> otShifts = PayrollCalculationFunctions.CalcDailyAndWeeklyOT(shifts, _standardOTSet);

                PayrollCalculationFunctions.CalcHolidayOT(otShifts, sunday.AddDays(2));

                OTShiftAssertions(otShifts[0], 8, 8, 0, 0);
                OTShiftAssertions(otShifts[1], 10, 8, 2, 0);
                OTShiftAssertions(otShifts[2], 14, 0, 12, 2);
            }

            [Fact]
            public void MultipleHolidaysTest()
            {
                DateTime sunday = new DateTime(637295460070000000);
                List<Shift> shifts = new List<Shift>();
                shifts.Add(new Shift(sunday, 8));
                shifts.Add(new Shift(sunday.AddDays(1), 10));
                shifts.Add(new Shift(sunday.AddDays(2), 12)); //holiday
                shifts.Add(new Shift(sunday.AddDays(3), 8));
                shifts.Add(new Shift(sunday.AddDays(4), 10)); //holiday
                shifts.Add(new Shift(sunday.AddDays(5), 14));  //holiday

                List<DateTime> holidays = new List<DateTime>();
                holidays.Add(sunday.AddDays(2));
                holidays.Add(sunday.AddDays(4));
                holidays.Add(sunday.AddDays(5));


                List<OTShift> otShifts = PayrollCalculationFunctions.CalcDailyAndWeeklyOT(shifts, _standardOTSet);

                PayrollCalculationFunctions.CalcHolidayOT(otShifts, holidays);

                OTShiftAssertions(otShifts[0], 8, 8, 0, 0);
                OTShiftAssertions(otShifts[1], 10, 8, 2, 0);
                OTShiftAssertions(otShifts[2], 12, 0, 12, 0);
                OTShiftAssertions(otShifts[3], 8, 8, 0, 0);
                OTShiftAssertions(otShifts[4], 10, 0, 10, 0);
                OTShiftAssertions(otShifts[5], 14, 0, 12, 2);
            }
        }

    }
}
